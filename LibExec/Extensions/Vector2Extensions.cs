using System.Numerics;

namespace LibExec.Extensions;

public static class Vector2Extensions
{
    public static Vector2 Normalize(this Vector2 value)
    {
        return Vector2.Normalize(value);
    }
}