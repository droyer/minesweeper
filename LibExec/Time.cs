namespace LibExec;

public sealed class Time
{
    internal Time()
    {
    }

    public static float DeltaTime { get; private set; }
    public static float TotalTime { get; private set; }
    public static int Fps { get; private set; }

    internal void SetDeltaTime(float value)
    {
        DeltaTime = value;
    }

    internal void SetTotalTime(float value)
    {
        TotalTime = value;
    }

    internal void SetFps(int value)
    {
        Fps = value;
    }
}