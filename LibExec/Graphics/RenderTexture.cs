namespace LibExec.Graphics;

public sealed class RenderTexture
{
    internal RenderTexture()
    {
    }

    public required uint Id { get; init; }
    public required Texture Texture { get; init; }
    public required Texture Depth { get; init; }
}