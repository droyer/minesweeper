namespace LibExec.Graphics;

public enum TextureFilter
{
    FilterPoint = 0,
    FilterBilinear,
    FilterTrilinear,
    FilterAnisotropic4X,
    FilterAnisotropic8X,
    FilterAnisotropic16X
}