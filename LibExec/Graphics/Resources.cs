using LibExec.Extensions;
using Raylib_cs;

namespace LibExec.Graphics;

public sealed class Resources
{
    private static Resources _instance = null!;
    private readonly string _baseDirectory;
    private readonly Dictionary<RenderTexture, RenderTexture2D> _renderTextures = new();
    private readonly Dictionary<Texture, Texture2D> _textures = new();

    internal Resources()
    {
        _instance = this;
        _baseDirectory = $"{AppDomain.CurrentDomain.BaseDirectory}";
    }

    public static Texture LoadTexture(string fileName)
    {
        var textureInternal = Raylib.LoadTexture(Path.Combine(_instance._baseDirectory, fileName));
        if (!Raylib.IsTextureReady(textureInternal))
        {
            throw new Exception($"Failed to open file '{fileName}'");
        }

        var texture = textureInternal.ToTexture();
        _instance._textures.Add(texture, textureInternal);
        return texture;
    }

    public static RenderTexture LoadRenderTexture(int width, int height)
    {
        var renderTextureInternal = Raylib.LoadRenderTexture(width, height);
        if (!Raylib.IsRenderTextureReady(renderTextureInternal))
        {
            throw new Exception("Failed to load the render texture");
        }

        var renderTexture = new RenderTexture
        {
            Id = renderTextureInternal.id,
            Texture = renderTextureInternal.texture.ToTexture(),
            Depth = renderTextureInternal.depth.ToTexture()
        };

        _instance._textures.Add(renderTexture.Texture, renderTextureInternal.texture);
        _instance._textures.Add(renderTexture.Depth, renderTextureInternal.depth);

        _instance._renderTextures.Add(renderTexture, renderTextureInternal);

        return renderTexture;
    }

    public static void UnloadRenderTexture(RenderTexture? renderTexture)
    {
        if (renderTexture is null || !_instance._renderTextures.TryGetValue(renderTexture, out var _)) return;

        Raylib.UnloadRenderTexture(GetRenderTextureInternal(renderTexture));
        _instance._renderTextures.Remove(renderTexture);
    }

    internal void UnloadTextures()
    {
        foreach (var texture in _textures.Values)
        {
            Raylib.UnloadTexture(texture);
        }
    }

    internal void UnloadRenderTextures()
    {
        foreach (var renderTextures in _renderTextures.Values)
        {
            Raylib.UnloadRenderTexture(renderTextures);
        }
    }

    internal static Texture2D GetTextureInternal(Texture texture)
    {
        return _instance._textures[texture];
    }

    internal static RenderTexture2D GetRenderTextureInternal(RenderTexture renderTexture)
    {
        return _instance._renderTextures[renderTexture];
    }
}