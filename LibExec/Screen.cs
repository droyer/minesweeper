using System.Drawing;
using System.Numerics;
using Raylib_cs;

namespace LibExec;

public sealed class Screen
{
    private const int DefaultScreenWidth = 800;
    private const int DefaultScreenWHeight = 600;
    private static Screen _instance = null!;
    private Size _lastSize;

    private Size _size = new(DefaultScreenWidth, DefaultScreenWHeight);

    public Screen()
    {
        _instance = this;
    }

    public static int Width => Raylib.IsWindowReady() ? Raylib.GetScreenWidth() : _instance._size.Width;
    public static int Height => Raylib.IsWindowReady() ? Raylib.GetScreenHeight() : _instance._size.Height;
    public static Size Size => new(Width, Height);
    public static string Title { get; set; } = "LibExec";
    public static Vector2 Center => new(Width / 2.0f, Height / 2.0f);
    public static event Action? SizeChanged;

    public static void SetSize(Size size)
    {
        _instance._size = size;

        if (Raylib.IsWindowReady())
        {
            Raylib.SetWindowSize(size.Width, size.Height);
        }
    }

    public static void SetSize(int width, int height)
    {
        SetSize(new Size(width, height));
    }

    public static void CenterWindow()
    {
        var screenWidth = Raylib.GetMonitorWidth(Raylib.GetCurrentMonitor());
        var screenHeight = Raylib.GetMonitorHeight(Raylib.GetCurrentMonitor());

        var x = (screenWidth - Width) / 2;
        var y = (screenHeight - Height) / 2;

        Raylib.SetWindowPosition(x, y);
    }

    public static void SetMinSize(int width, int height)
    {
        Raylib.SetWindowMinSize(width, height);
    }

    public static void SetMinSize(Size size)
    {
        Raylib.SetWindowMinSize(size.Width, size.Height);
    }

    internal void Update()
    {
        // todo: add args and handle resize by the user
        if (Raylib.IsWindowResized() && _size != _lastSize)
        {
            SizeChanged?.Invoke();
            _lastSize = _size;
        }
    }
}