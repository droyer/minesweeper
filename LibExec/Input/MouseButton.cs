namespace LibExec.Input;

public enum MouseButton
{
    Left,
    Right,
    Middle,
    Side,
    Extra,
    Forward,
    Back
}