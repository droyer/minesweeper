using System.Diagnostics;

namespace LibExec;

public sealed class DispatcherFrame
{
    private const int FpsCaptureFramesCount = 30;
    private const float FpsAverageTimeSeconds = 0.5f;
    private const double FpsStep = FpsAverageTimeSeconds / FpsCaptureFramesCount;

    private readonly Stopwatch _gameTimer = new();
    private readonly float[] _histories = new float[FpsCaptureFramesCount];
    private readonly TimeSpan _maxElapsedTime = TimeSpan.FromMilliseconds(500);
    private readonly TimeSpan _targetElapsedTime = TimeSpan.FromTicks(166667); // 60fps

    private readonly Time _time = new();
    private TimeSpan _accumulatedElapsedTime;
    private float _average;
    private int _index;
    private float _last;
    private long _previousTicks;

    public Action? OnDraw;
    public Action? OnUpdate;

    public void Start()
    {
        _gameTimer.Start();
    }

    public void Update()
    {
        RetryTick:

        var currentTicks = _gameTimer.Elapsed.Ticks;
        _accumulatedElapsedTime += TimeSpan.FromTicks(currentTicks - _previousTicks);
        _previousTicks = currentTicks;

        if (_accumulatedElapsedTime < _targetElapsedTime)
        {
            var sleepTime = (_targetElapsedTime - _accumulatedElapsedTime).TotalMilliseconds;
            if (sleepTime >= 2.0)
            {
                Thread.Sleep(1);
            }

            goto RetryTick;
        }

        if (_accumulatedElapsedTime > _maxElapsedTime)
        {
            _accumulatedElapsedTime = _maxElapsedTime;
        }

        _time.SetDeltaTime((float)_accumulatedElapsedTime.TotalSeconds);
        _time.SetTotalTime(Time.TotalTime + (float)_accumulatedElapsedTime.TotalSeconds);

        _accumulatedElapsedTime = TimeSpan.Zero;
        UpdateFps();

        OnUpdate?.Invoke();
        OnDraw?.Invoke();
    }

    private void UpdateFps()
    {
        if (Time.DeltaTime == 0)
        {
            _time.SetFps(0);
        }

        var currentTime = _gameTimer.Elapsed.TotalSeconds;
        if (currentTime - _last > FpsStep)
        {
            _last = (float)currentTime;
            _index = (_index + 1) % FpsCaptureFramesCount;
            _average -= _histories[_index];
            _histories[_index] = Time.DeltaTime / FpsCaptureFramesCount;
            _average += _histories[_index];
        }

        _time.SetFps((int)MathF.Round(1.0f / _average));
    }
}