namespace Minesweeper.Enums;

public enum Difficulty
{
    Easy,
    Medium,
    Hard
}