using System.Drawing;
using LibExec.Graphics;
using LibExec.Input;

namespace Minesweeper;

public sealed class Block
{
    public const int Size = 32;
    private readonly RectangleF _destinationRectangle;
    private int _bombCount;
    private bool _clicking;
    private bool _hover;
    private bool _isDiscover;
    private RectangleF _sourceRectangle;

    public Action<int, int> OnClick = null!;

    public Block(int x, int y)
    {
        X = x;
        Y = y;

        _sourceRectangle = UnRevealRectangle;
        _destinationRectangle = new RectangleF(X * Size, Y * Size, Size, Size);
    }

    public int X { get; }
    public int Y { get; }
    public bool IsBomb { get; set; }
    public bool IsFlag { get; private set; }

    private static RectangleF UnRevealRectangle => GetSourceRectangle(0, 0);
    private static RectangleF RevealingRectangle => GetSourceRectangle(1, 0);
    private static RectangleF BombRectangle => GetSourceRectangle(0, 2);
    private static RectangleF FlagRectangle => GetSourceRectangle(0, 1);
    private static RectangleF Bomb0Rectangle => GetSourceRectangle(3, 2);
    private static RectangleF Bomb1Rectangle => GetSourceRectangle(0, 3);
    private static RectangleF Bomb2Rectangle => GetSourceRectangle(1, 3);
    private static RectangleF Bomb3Rectangle => GetSourceRectangle(2, 3);
    private static RectangleF Bomb4Rectangle => GetSourceRectangle(3, 3);
    private static RectangleF Bomb5Rectangle => GetSourceRectangle(0, 4);
    private static RectangleF Bomb6Rectangle => GetSourceRectangle(1, 4);
    private static RectangleF Bomb7Rectangle => GetSourceRectangle(2, 4);
    private static RectangleF Bomb8Rectangle => GetSourceRectangle(3, 4);
    private static RectangleF BombSelectionRectangle => GetSourceRectangle(2, 2);
    private static RectangleF BombFlagRectangle => GetSourceRectangle(1, 2);

    public void Update()
    {
        _hover = Board.MouseRectangle.IntersectsWith(_destinationRectangle) && !_isDiscover;

        if (_hover && Input.IsMouseDown(MouseButton.Left) && !IsFlag)
        {
            _clicking = true;
            _sourceRectangle = RevealingRectangle;
        }

        if (_clicking && Input.IsMouseUp(MouseButton.Left))
        {
            _clicking = false;
            _sourceRectangle = UnRevealRectangle;
            if (_hover)
            {
                OnClick(X, Y);
            }
        }

        if (_hover && Input.IsMouseDown(MouseButton.Right))
        {
            IsFlag = !IsFlag;
            _sourceRectangle = IsFlag ? FlagRectangle : UnRevealRectangle;
        }
    }

    public void Draw(Texture texture)
    {
        texture.Draw(_sourceRectangle, _destinationRectangle);
        if (_hover && !_clicking)
        {
            Renderer.DrawRectangle(_destinationRectangle, Color.FromArgb(20, 20, 20, 20));
        }
    }

    public void UpdateBombCount()
    {
        for (var x = -1; x <= 1; x++)
        {
            for (var y = -1; y <= 1; y++)
            {
                if (x == 0 && y == 0)
                {
                    continue;
                }

                var xx = X + x;
                var yy = Y + y;

                var block = Board.GetBlock(xx, yy);
                if (block is null)
                {
                    continue;
                }

                if (block.IsBomb)
                {
                    _bombCount++;
                }
            }
        }
    }

    public void Discover()
    {
        _isDiscover = true;
        _hover = false;
        IsFlag = false;
        UpdateBombCountRectangle();

        if (_bombCount != 0)
        {
            return;
        }

        for (var x = -1; x <= 1; x++)
        {
            for (var y = -1; y <= 1; y++)
            {
                if (x == 0 && y == 0)
                {
                    continue;
                }

                var xx = X + x;
                var yy = Y + y;

                var block = Board.GetBlock(xx, yy);

                if (block is { _isDiscover: false, IsBomb: false })
                {
                    block.Discover();
                }
            }
        }
    }

    public void Loose()
    {
        _sourceRectangle = BombSelectionRectangle;
    }

    public void DiscoverBomb()
    {
        _sourceRectangle = IsBomb switch
        {
            false when IsFlag => BombFlagRectangle,
            true when !IsFlag => BombRectangle,
            _ => _sourceRectangle
        };
    }

    public bool IsRevealed()
    {
        return IsBomb || _isDiscover;
    }

    private void UpdateBombCountRectangle()
    {
        _sourceRectangle = _bombCount switch
        {
            0 => Bomb0Rectangle,
            1 => Bomb1Rectangle,
            2 => Bomb2Rectangle,
            3 => Bomb3Rectangle,
            4 => Bomb4Rectangle,
            5 => Bomb5Rectangle,
            6 => Bomb6Rectangle,
            7 => Bomb7Rectangle,
            8 => Bomb8Rectangle,
            _ => _sourceRectangle
        };
    }

    private static RectangleF GetSourceRectangle(int x, int y)
    {
        const int width = 247;
        const int height = 247;
        return new RectangleF(5 + (width + 10) * x, 5 + (height + 10) * y, width, height);
    }
}