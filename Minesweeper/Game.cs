using System.Drawing;
using LibExec;
using LibExec.Graphics;
using LibExec.Input;
using Minesweeper.Enums;

namespace Minesweeper;

public class Game : Application
{
    private readonly Board _board = new();

    protected override void Start()
    {
        _board.Start();
    }

    protected override void Update()
    {
        if (Input.IsKeyDown(Key.Escape))
        {
            Exit();
        }

        if (Input.IsKeyDown(Key.F1))
        {
            _board.LoadGame(Difficulty.Easy);
        }

        if (Input.IsKeyDown(Key.F2))
        {
            _board.LoadGame(Difficulty.Medium);
        }

        if (Input.IsKeyDown(Key.F3))
        {
            _board.LoadGame(Difficulty.Hard);
        }

        _board.Update();
    }

    protected override void Draw()
    {
        _board.DrawToRenderTarget();

        Renderer.Begin();
        Renderer.Clear(Color.Black);

        _board.DrawRenderTarget();

        Renderer.End();
    }
}