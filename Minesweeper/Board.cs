using System.Drawing;
using System.Numerics;
using LibExec;
using LibExec.Graphics;
using LibExec.Input;
using Minesweeper.Enums;

namespace Minesweeper;

public sealed class Board
{
    private static Board _instance = null!;
    private Block[,] _blocks = null!;
    private Difficulty _difficulty;
    private bool _hasLoose;
    private bool _hasWin;
    private int _height = 8;
    private bool _isGenerated;
    private bool _isStarted;
    private Vector2 _oldMousePosition;
    private float _scale;
    private bool _showWin;
    private Size _size;

    private bool _switching;
    private RenderTexture _target = null!;
    private Texture _texture = null!;
    private int _totalBombCount;
    private int _width = 8;

    public Board()
    {
        _instance = this;
    }

    public static RectangleF MouseRectangle { get; private set; }

    public void Start()
    {
        _texture = Resources.LoadTexture("Assets/minesweeper.png");
        Screen.SizeChanged += Screen.CenterWindow;
        LoadGame(Difficulty.Easy);
        _isStarted = false;
    }

    public void Update()
    {
        _scale = MathF.Min((float)Screen.Width / _size.Width, (float)Screen.Height / _size.Height);

        switch (_switching)
        {
            case false:
                MouseRectangle = new RectangleF
                {
                    X = (Input.MousePosition.X - (Screen.Width - _size.Width * _scale) * 0.5f) / _scale,
                    Y = (Input.MousePosition.Y - (Screen.Height - _size.Height * _scale) * 0.5f) / _scale,
                    Width = 0.1f / _scale,
                    Height = 0.1f / _scale
                };
                break;
            case true when Vector2.Distance(Input.MousePosition, _oldMousePosition) > 1:
                _switching = false;
                break;
        }

        if (!_hasLoose && !_hasWin && _isStarted)
        {
            foreach (var block in _blocks)
            {
                block.Update();
            }
        }

        if (_hasLoose || _hasWin)
        {
            if (Input.IsKeyDown(Key.Enter) || Input.IsKeyDown(Key.Space))
            {
                LoadGame(_difficulty, false);
            }
        }
    }

    public void DrawToRenderTarget()
    {
        Renderer.BeginTextureMode(_target);
        Renderer.Clear(Color.Black);

        foreach (var block in _blocks)
        {
            block.Draw(_texture);
        }

        if (!_isStarted)
        {
            var whiteColor = Color.FromArgb(255, 200, 200, 200);
            Renderer.DrawRectangle(new RectangleF(0, 0, Screen.Width, Screen.Height), Color.FromArgb(200, 20, 20, 20));

            Renderer.DrawText("Minesweeper", new Vector2(20, 20), 40, whiteColor);

            Renderer.DrawText("F1: Easy 9x9 10 bombs", new Vector2(35, 100), 20, whiteColor);
            Renderer.DrawText("F2: Medium 16x16 40 bombs", new Vector2(15, 130), 20, whiteColor);
            Renderer.DrawText("F3: Hard 30x16 99 bombs", new Vector2(20, 160), 20, whiteColor);
        }

        if (_showWin)
        {
            var whiteColor = Color.FromArgb(255, 200, 200, 200);
            Renderer.DrawRectangle(new RectangleF(0, 0, Screen.Width, Screen.Height), Color.FromArgb(200, 20, 20, 20));

            Renderer.DrawText("Win !", new Vector2(_size.Width / 2f - 42, _size.Height / 2f - 18), 40, whiteColor);
        }

        Renderer.EndTextureMode();
    }

    public void DrawRenderTarget()
    {
        _target.Texture.Draw(
            new RectangleF(0, 0, _target.Texture.Width, -_target.Texture.Height),
            new RectangleF((Screen.Width - _size.Width * _scale) * 0.5f,
                (Screen.Height - _size.Height * _scale) * 0.5f,
                _size.Width * _scale, _size.Height * _scale));
    }

    public void LoadGame(Difficulty difficulty, bool updateScreen = true)
    {
        _isGenerated = false;
        _hasLoose = false;
        _hasWin = false;
        _isStarted = true;
        _showWin = false;

        _difficulty = difficulty;

        var numBlocks = difficulty switch
        {
            Difficulty.Easy => new Size(9, 9),
            Difficulty.Medium => new Size(16, 16),
            Difficulty.Hard => new Size(30, 16),
            _ => throw new ArgumentOutOfRangeException(nameof(difficulty), difficulty, null)
        };

        _width = numBlocks.Width;
        _height = numBlocks.Height;

        _size = new Size(_width * Block.Size, _height * Block.Size);

        Screen.SetMinSize(_size);
        if (updateScreen)
        {
            Screen.SetSize((int)(_size.Width * 1.5f), (int)(_size.Height * 1.5f));
        }

        Resources.UnloadRenderTexture(_target);
        _target = Resources.LoadRenderTexture(_size.Width, _size.Height);
        _target.Texture.SetFilter(TextureFilter.FilterPoint);

        var numBombs = difficulty switch
        {
            Difficulty.Easy => 10,
            Difficulty.Medium => 40,
            Difficulty.Hard => 99,
            _ => throw new ArgumentOutOfRangeException(nameof(difficulty), difficulty, null)
        };

        Generate(numBombs);

        // workaround to avoid old case to be clickable
        _switching = true;
        MouseRectangle = new RectangleF();
        _oldMousePosition = Input.MousePosition;
    }

    public static Block? GetBlock(int x, int y)
    {
        if (x < 0 || y < 0 || x >= _instance._width || y >= _instance._height)
        {
            return null;
        }

        return _instance._blocks[x, y];
    }

    private void Generate(int bombNumber)
    {
        _totalBombCount = bombNumber;

        _blocks = new Block[_width, _height];

        for (var x = 0; x < _width; x++)
        {
            for (var y = 0; y < _height; y++)
            {
                _blocks[x, y] = new Block(x, y)
                {
                    OnClick = OnBlockClick
                };
            }
        }
    }

    private void OnBlockClick(int x, int y)
    {
        if (!_isGenerated)
        {
            GenerateBombs(_blocks[x, y]);
            UpdateBlocksBombCount();
            _isGenerated = true;
        }

        var block = _blocks[x, y];

        if (block.IsBomb)
        {
            Loose(block);
            _hasLoose = true;
        }
        else
        {
            block.Discover();
            CheckWin();
        }
    }

    private void GenerateBombs(Block selectedBlock)
    {
        var indexes = new List<int>(_blocks.Length);
        for (var x = 0; x < _width; x++)
        {
            for (var y = 0; y < _height; y++)
            {
                if (x >= selectedBlock.X - 1 && x <= selectedBlock.X + 1 && y >= selectedBlock.Y - 1 &&
                    y <= selectedBlock.Y + 1)
                {
                    continue;
                }

                var index = x + y * _width;
                indexes.Add(index);
            }
        }

        for (var i = 0; i < _totalBombCount; i++)
        {
            var index = indexes[Random.Shared.Next(indexes.Count)];

            var x = index % _width;
            var y = index / _width;

            var block = _blocks[x, y];
            block.IsBomb = true;
            indexes.Remove(index);
        }
    }

    private void UpdateBlocksBombCount()
    {
        foreach (var block in _blocks)
        {
            block.UpdateBombCount();
        }
    }

    private void Loose(Block selectedBlock)
    {
        selectedBlock.Loose();

        foreach (var block in _blocks)
        {
            if (block == selectedBlock)
            {
                continue;
            }

            block.DiscoverBomb();
        }
    }

    private void CheckWin()
    {
        if (_blocks.Cast<Block>().Any(block => !block.IsRevealed()))
        {
            return;
        }

        _hasWin = true;
        Task.Run(async () =>
        {
            await Task.Delay(500);

            _showWin = true;
        });
    }
}